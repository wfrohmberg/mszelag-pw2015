/**
 * 
 */
package snippet;

/**
 * @author mateusz.szelag
 *
 */
public class Box{

	private int value;
	private int weight;
	
	/**
	 * Default constructor
	 */
	public Box(){
		value 	= 0;
		weight 	= 0;
	}
	
	/**
	 * Constructor
	 * 
	 * @param weight of box
	 * @param value of box
	 */
	public Box(int weight, int value) {
		this();
		this.value 	= value;
		this.weight = weight;
	}

	/**
	 * 
	 * @return value of box
	 */
	public int getValue() {
		return value;
	}
	
	/**
	 * 
	 * @param value of box
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * 
	 * @return weight of box
	 */
	public int getWeight() {
		return weight;
	}
	
	/**
	 * 
	 * @param weight of box
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Weight: " + weight + " Value: " + value;
	}

}
