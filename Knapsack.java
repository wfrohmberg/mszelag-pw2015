/**
 * 
 */
package snippet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author mateusz.szelag
 *
 */
public abstract class Knapsack<E> implements Iterable<E>{
	
	private E[] 			boxes;
	private int 			capacity; 
	
	/**
	 * 
	 * @param capacity
	 * @param boxes
	 */
	@SuppressWarnings("unchecked")
	public Knapsack(int capacity, Collection<E> boxes) {
		this.capacity 	= capacity;
		this.boxes = (E[]) boxes.toArray();
	}
	
	/**
	 * 
	 * @param capacity
	 * @param boxes
	 */
	public Knapsack(int capacity, E[] boxes) {
		this.capacity 	= capacity;
		this.boxes 		= boxes;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public Collection<E> Pack() throws Exception{
		
		System.out.println("Boxes: "+boxes.length);
		
		int N = boxes.length;   // number of items
        int W = capacity;   	// maximum weight of knapsack
        
		// opt[n][w] = max profit of packing items 1..n with weight limit w
        // sol[n][w] = does opt solution to pack items 1..n with weight limit w include item n?
        int[][] 	opt = new int[N+1][W+1];
        boolean[][] sol = new boolean[N+1][W+1];

        for (int n = 1; n <= N; n++) {
            for (int w = 1; w <= W; w++) {

                // don't take item n
                int option1 = opt[n-1][w];

                // take item n
                int option2 = Integer.MIN_VALUE;
                
                if (getWeight(boxes[n-1]) <= w) {
                	option2 = getValue(boxes[n-1]) + opt[n-1][w-getWeight(boxes[n-1])];
                }

                // select better of two options
                opt[n][w] = Math.max(option1, option2);
                sol[n][w] = (option2 > option1);
            }
        }
        
        List<E> result = new ArrayList<E>();
        
        // determine which items to take
        boolean[] take = new boolean[N+1];
        for (int n = N, w = W; n > 0; n--) {
            if (sol[n][w]) {
            	take[n] = true;  
            	w = w - getWeight(boxes[n-1]);
            	result.add(boxes[n-1]);
            } else {
            	take[n] = false;
            }
        }
        
        return result;     
        
	}

	/**
	 * @param object
	 * @return
	 */
	protected abstract int getWeight(E box) throws Exception;
	
	/**
	 * @param object
	 * @return
	 */
	protected abstract int getValue(E box) throws Exception;

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>(){

			private int index = 0;
			
			@Override
			public boolean hasNext() {
				return index < boxes.length;
			}

			@Override
			public E next() {
				index++;
				return boxes[index-1];
			}
			
		};
	}
}