/**
 * 
 */
package snippet;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author mateusz.szelag
 *
 */
public class Reflection {
	
	public static List<Class<?>> getClassesFromJar(File file) throws Exception{
		
		List<Class<?>> result = new ArrayList<Class<?>>();
		
    	URL url = file.toURI().toURL();
    	URLClassLoader classLoader = new URLClassLoader(new URL[]{url}, System.class.getClassLoader());

	    JarFile jar = new JarFile(file);
	    		
        Enumeration<JarEntry> allEntries = jar.entries();
        while (allEntries.hasMoreElements()) {
            JarEntry entry = allEntries.nextElement();
            if (entry.getName().endsWith(".class") /*&& !entry.getName().contains("$")*/) {
            	
            	String className = entry.getName().replaceAll("/", "\\.");
            	className = className.replace(".class", "");
            		
            	try{
	            	Class<?> c = classLoader.loadClass(className);
	            	if(c != null){
	            		System.out.println("Load class: "+c);
		            	result.add(c);
	            	}
            	} catch (java.lang.NoClassDefFoundError e){
            		//just omit when error error
            	}
            	
            }
            	
        }
        
        classLoader.close();
        jar.close();
		
		return result;
	}
	
}
