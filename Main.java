/**
 * 
 */
package snippet;

import java.awt.Dimension;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.text.NumberFormatter;


/**
 * @author mateusz.szelag
 *
 * Zadanie:
 *
 * http://www.cs.put.poznan.pl/wfrohmberg/download/projekty-pw2014-3.pdf
 * 
 * 3.Standardowy problem plecakowy(na ocenę 4.0)
 * Zadaniem jest stworzenie klasy rozwiązującej problem plecakowy w czasie pseudowielomianowym
 * dla obiektów dowolnego typu.
 * 
 * Przykładowy sposób użycia klasy
 * 
 * List<Box> boxes = new List<Box>();
 * boxes.Add(new Box(3, 5)); // Box(weight, value);
 * boxes.Add(new Box(2, 2));
 * boxes.Add(new Box(5, 2)); 
 * // ...
 * Knapsack<Box> knapsack = boxes.ToKnapsack(32, box => box.Weight, box => box.Value); 
 * // ToKnapsack(capacity, weight lambda, value lambda)
 * knapsack.Pack();
 * foreach (Box box in knapsack)
 * {
 *     Console.WriteLine("{0} {1}", box.Weight, box.Value);
 * }
 * 
 * Przy czym metoda ToKnapsack powinna działać na dowolnego typu kolekcji (w tym tablicy).
 * Ponadto należy stworzyć WPF-owy interfejs umożliwiający wybranie przez użytkownika 
 * dowolnego typu z dowolnej biblioteki klas zaimplementowanej w Visual Studio i zainicjowanie 
 * wybranej liczby obiektów tej klasy wraz z możliwością ustawienia wartości wszystkich 
 * właściwości tych obiektów oraz załadowanie na wybraną (również przez użytkownika) kolekcję, 
 * czy to dostępną w wybranej przez użytkownika bibliotece klas czy też dostępną w bibliotekach 
 * systemowych. Należy tutaj zapewnić możliwość wyboru metody dodającej obiekt do kolekcji (jeśli 
 * wybrana kolekcja nie jest tablicą). Oraz umożliwić użytkownikowi uruchomienie algorytmu 
 * dynamicznego programowania zaimplementowanego w klasie Knapsack.
 *
 */
public class Main {
	
	/**
	 * Entry method
	 */
	public static void main(String[] args) {
		
		try {
			Main main = new Main();
			main.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void run() throws Exception{
		
		/**
		 * Test for generic snapsack
		 */
		//knapsackGenericTest();
		
		/**
		 * Test for generic snapsack using reflection
		 */
		knapsackReflectionTest();

	}

	/**
	 * 
	 */
	private void knapsackReflectionTest() throws Exception{
		
		MainFrame mainFrame = new MainFrame(){
			@Override
			protected Collection<Class<?>> loadJar(File file) {
				try {
					return Arrays.asList(Reflection.getClassesFromJar(file).stream()
							.filter( (objClass) -> { //only classes with default constructor
			    				return Arrays.asList(objClass.getConstructors()).stream().anyMatch(
			    						(constructor) -> {
											return constructor.getParameterCount() == 0;
			    						}) 
			    						&& // AND only classes which contains at least 1 method that takes int
			    						Arrays.asList(objClass.getMethods()).stream().anyMatch(
			    								(method) -> {
			    									return method.getParameterCount() == 1 //param count == 1
			    											&& // AND param type equals int
			    											Arrays.asList(method.getParameters()).stream().anyMatch(
			    				    								(objClassParam) -> {
			    				    									return objClassParam.getType().equals(int.class);
			    						    				});
					    				}) 
					    				&& // AND only classes which contains at least 1 method that return int
					    				Arrays.asList(objClass.getMethods()).stream().anyMatch(
			    								(method) -> {
			    									return method.getReturnType().equals(int.class);
					    				})
					    				;})
			    			.toArray(Class<?>[]::new));
				} catch (Exception e) {
					e.printStackTrace();
					return new ArrayList<Class<?>>();
				}
			}

			@Override
			protected Collection<Constructor<?>> filterModelConstructors(Class<?> classObject) {
				//lista konstruktorów (domyślnych) dla danej klasy
				return Arrays.asList(Arrays.asList(classObject.getConstructors()).stream().filter((constructor) -> {
					return constructor.getParameterCount() == 0;
				}).toArray(Constructor<?>[]::new));
			}

			@Override
			protected Collection<Method> filterModelGetValue(Class<?> classObject) {
				//tylko metody zwracające int
				return Arrays.asList(Arrays.asList(classObject.getMethods()).stream().filter((method) -> {
					return method.getReturnType().equals(int.class);
				}).toArray(Method[]::new));
			}

			@Override
			protected Collection<Method> filterModelGetWeight(Class<?> classObject) {
				//tylko metody zwracające int
				return Arrays.asList(Arrays.asList(classObject.getMethods()).stream().filter((method) -> {
					return method.getReturnType().equals(int.class);
				}).toArray(Method[]::new));
			}

			@Override
			protected Collection<Method> filterModelSetWeight(Class<?> classObject) {
				//tylko metody przyjmujące jednego inta
				return Arrays.asList(Arrays.asList(classObject.getMethods()).stream().filter((method) -> {
					return method.getParameterCount() == 1 //param count == 1
							&& // AND param type equals int
							Arrays.asList(method.getParameters()).stream().anyMatch(
    								(objClassParam) -> {
    									return objClassParam.getType().equals(int.class);
		    				});
				}).toArray(Method[]::new));
			}

			@Override
			protected Collection<Method> filterModelSetValue(Class<?> classObject) {
				//tylko metody przyjmujące jednego inta
				return Arrays.asList(Arrays.asList(classObject.getMethods()).stream().filter((method) -> {
					return method.getParameterCount() == 1 //param count == 1
							&& // AND param type equals int
							Arrays.asList(method.getParameters()).stream().anyMatch(
    								(objClassParam) -> {
    									return objClassParam.getType().equals(int.class);
		    				});
				}).toArray(Method[]::new));
			}

			@Override
			protected Object newBoxInstance(Class<?> classObject, Constructor<?> constructor, Method setWeight, Method setValue) {
				
				NumberFormat format = NumberFormat.getInstance();
				format.setGroupingUsed(false);
			    NumberFormatter formatter = new NumberFormatter(format);
			    formatter.setValueClass(Integer.class);
			    formatter.setMinimum(0);
			    formatter.setMaximum(Integer.MAX_VALUE);
			    // If you want the value to be committed on each keystroke instead of focus lost
			    formatter.setCommitsOnValidEdit(true);
			    JFormattedTextField weight 	= new JFormattedTextField(formatter);
			    JFormattedTextField value 	= new JFormattedTextField(formatter);
			    

			    final JComponent[] inputs = new JComponent[] {
			    		new JLabel("Weight"),
			    		weight,
			    		new JLabel("Value"),
			    		value
			    };
			    JOptionPane.showMessageDialog(null, inputs, "New Box instance", JOptionPane.PLAIN_MESSAGE);
			    
			    try {
			    	if(value.getText().length() > 0 && weight.getText().length() > 0){
			    		Object box = constructor.newInstance();
						setWeight.invoke(box, Integer.parseInt(weight.getText()));
						setValue.invoke(box, Integer.parseInt(value.getText()));
						return box;
			    	}
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			    
				return null;
			}

			@Override
			protected Collection<Object> knapsackPack(int capacity,  Method getWeight, Method getValue, Collection<Object> boxes) {
				
				Knapsack<Object> knapsack = new Knapsack<Object>(capacity, boxes){

					@Override
					protected int getWeight(Object box) throws Exception {
						return (int) getWeight.invoke(box);
					}

					@Override
					protected int getValue(Object box) throws Exception {
						return (int) getValue.invoke(box);
					}
					
				};
				
				try {
					return knapsack.Pack();
				} catch (Exception e) {
					e.printStackTrace();
					return new ArrayList<Object>();
				}
			}


		};
		
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setPreferredSize(new Dimension(800, 600));
		mainFrame.pack();
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
		

	}


	/**
	 * @throws Exception 
	 * 
	 */
	@SuppressWarnings("unused")
	private void knapsackGenericTest() throws Exception {
		
		System.out.print("generic test on collection");
		
		Collection<Box> collection = new ArrayList<Box>();
		collection.add(new Box(1, 4)); // Box(weight, value);
		collection.add(new Box(2, 2));
		collection.add(new Box(1, 3)); 

		Knapsack<Box> knapsack = new Knapsack<Box>(3, collection){

			@Override
			protected int getWeight(Box box) throws Exception {
				return box.getWeight();
			}

			@Override
			protected int getValue(Box box) throws Exception {
				return box.getValue();
			}

		};

		knapsack.Pack().forEach((box) -> System.out.println(box));
		
		System.out.print("generic test on collection");
	
		Box[] array = new Box[]{
				new Box(1, 4), 
				new Box(2, 2), 
				new Box(1, 3)
				};
		
		knapsack = new Knapsack<Box>(3, array){

			@Override
			protected int getWeight(Box box) throws Exception {
				return box.getWeight();
			}

			@Override
			protected int getValue(Box box) throws Exception {
				return box.getValue();
			}

		};

		knapsack.Pack().forEach((box) -> System.out.println(box));
		
	}
	
	
}

