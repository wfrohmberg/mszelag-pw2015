/**
 * 
 */
package snippet;

import javax.swing.JFrame;

import net.miginfocom.swing.MigLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.NumberFormatter;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.ListSelectionModel;

/**
 * @author mateusz.szelag
 *
 */
public abstract class MainFrame extends JFrame{
	
	private DefaultListModel<Class<?>> 			modelClasses;
	private DefaultListModel<Constructor<?>> 	modelConstructors;
	private DefaultListModel<Method> 			modelMethodGetValue;
	private DefaultListModel<Method> 			modelMethodSetValue;
	private DefaultListModel<Method> 			modelMethodGetWeight;
	private DefaultListModel<Method> 			modelMethodSetWeight;
	private DefaultListModel<Object> 			modelInstances;
	private DefaultListModel<Object> 			modelResult;
	
	private JScrollPane 						getValueMethodScrollPanel;
	private JList<Method> 						getValueMethodList;
	private JList<Method> 						setValueMethodList;
	private JLabel 								getValueLabel;
	private JList<Constructor<?>> 				constructorList;
	private JScrollPane 						scrollPanelConstructorList;
	private JLabel							 	constructorsLabel;
	private JPanel 								centerPanel;
	private JPanel 								leftPanel;
	private JList<Class<?>> 					classList;
	private JLabel 								setWeightLabel;
	private JScrollPane 						setWeightMethodScrollPanel;
	private JLabel 								getWeightLabel;
	private JScrollPane 						getWeightMethodScrollPanel;
	private JList<Method> 						getWeightMethodList;
	private JList<Method> 						setWeightMethodList;
	private JScrollPane 						scrollPaneClassList;
	private JLabel 								classesLabel;
	private JLabel 								setValueLabel;
	private JScrollPane 						setValueMethodScrollPanel;
	private JPanel 								rightPanel;
	private JButton 							newInstanceButton;
	private JLabel 								instancjeLabel;
	private JScrollPane 						instancesListScrollPane;
	private JList<Object> 						instancesList;
	private JList<Object> 						resultList;
	private JPanel 								resultPanel;
	private JButton 							packButton;
	private JScrollPane 						resultListLabel;
	private JLabel 								packLabel;
	
	public MainFrame() {
		getContentPane().setLayout(new MigLayout("", "[grow][grow][grow][grow]", "[grow]"));
		
		leftPanel = new JPanel();
		leftPanel.setLayout(new MigLayout("", "[grow]", "[][grow]"));
		getContentPane().add(leftPanel, "cell 0 0,grow");
		
		classesLabel = new JLabel("Klasa");
		leftPanel.add(classesLabel);
		
		scrollPaneClassList = new JScrollPane();
		leftPanel.add(scrollPaneClassList, "cell 0 1,grow");
		
		classList = new JList<Class<?>>();
		classList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		classList.addListSelectionListener((evt) -> {
			
			modelConstructors.removeAllElements();
			modelMethodGetValue.removeAllElements();
			modelMethodGetWeight.removeAllElements();
			modelMethodSetValue.removeAllElements();
			modelMethodSetWeight.removeAllElements();
			
			if(classList.getSelectedIndex() >= 0){
				
				//lista konstruktorów
				filterModelConstructors(modelClasses.get(classList.getSelectedIndex())).forEach((constructor) -> {
					modelConstructors.add(0, constructor);
				});
				
				//metody get value
				filterModelGetValue(modelClasses.get(classList.getSelectedIndex())).stream().forEach((method) -> {
					modelMethodGetValue.add(0, method);
				});
				
				//metody get weight
				filterModelGetWeight(modelClasses.get(classList.getSelectedIndex())).stream().forEach((method) -> {
					modelMethodGetWeight.add(0, method);
				});
				
				//metody set value
				filterModelSetValue(modelClasses.get(classList.getSelectedIndex())).stream().forEach((method) -> {
					modelMethodSetValue.add(0, method);
				});
				
				//metody set weight
				filterModelSetWeight(modelClasses.get(classList.getSelectedIndex())).stream().forEach((method) -> {
					modelMethodSetWeight.add(0, method);
				});
				
			}
	    });
		modelClasses = new DefaultListModel<Class<?>>();
		classList.setModel(modelClasses);
		scrollPaneClassList.setViewportView(classList);
		
		centerPanel = new JPanel();
		centerPanel.setLayout(new MigLayout("", "[grow]", "[][][][][][grow][][][][grow][][]"));
		
		constructorsLabel = new JLabel("Konstruktor");
		centerPanel.add(constructorsLabel, "cell 0 0");
		
		scrollPanelConstructorList = new JScrollPane();
		centerPanel.add(scrollPanelConstructorList, "cell 0 1,growx");
		getContentPane().add(centerPanel, "cell 1 0,grow");
		
		constructorList = new JList<Constructor<?>>();
		constructorList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		modelConstructors = new DefaultListModel<Constructor<?>>();
		constructorList.setModel(modelConstructors);
		scrollPanelConstructorList.setViewportView(constructorList);
		
		getValueLabel = new JLabel("Metoda int getValue()");
		centerPanel.add(getValueLabel, "cell 0 2");
		
		getValueMethodScrollPanel = new JScrollPane();
		centerPanel.add(getValueMethodScrollPanel, "cell 0 3,grow");
		
		setValueLabel = new JLabel("Metoda int setValue()");
		centerPanel.add(setValueLabel, "cell 0 4");
		
		setValueMethodList = new JList<Method>();
		modelMethodSetValue = new DefaultListModel<Method>();
		setValueMethodList.setModel(modelMethodSetValue);
		setValueMethodList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setValueMethodScrollPanel = new JScrollPane(setValueMethodList);
		centerPanel.add(setValueMethodScrollPanel, "cell 0 5,grow");
		
		getWeightLabel = new JLabel("Metoda int getWeight()");
		centerPanel.add(getWeightLabel, "cell 0 6");
		
		getValueMethodList = new JList<Method>();
		modelMethodGetValue = new DefaultListModel<Method>();
		getValueMethodList.setModel(modelMethodGetValue);
		getValueMethodList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getValueMethodScrollPanel.setViewportView(getValueMethodList);
		
		getWeightMethodScrollPanel = new JScrollPane();
		getWeightMethodList = new JList<Method>();
		modelMethodGetWeight = new DefaultListModel<Method>();
		getWeightMethodList.setModel(modelMethodGetWeight);
		getWeightMethodList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getWeightMethodScrollPanel.setViewportView(getWeightMethodList);
		centerPanel.add(getWeightMethodScrollPanel, "cell 0 7,grow");
		
		setWeightLabel = new JLabel("Metoda int setWeight()");
		centerPanel.add(setWeightLabel, "cell 0 8");
		
		setWeightMethodList = new JList<Method>();
		modelMethodSetWeight = new DefaultListModel<Method>();
		setWeightMethodList.setModel(modelMethodSetWeight);
		setWeightMethodList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setWeightMethodScrollPanel = new JScrollPane(setWeightMethodList);
		centerPanel.add(setWeightMethodScrollPanel, "cell 0 9,grow");
		
		rightPanel = new JPanel();
		rightPanel.setLayout(new MigLayout("", "[grow]", "[][][grow]"));
		
		getContentPane().add(rightPanel, "cell 2 0,grow");
		
		instancjeLabel = new JLabel("Instancje");
		rightPanel.add(instancjeLabel, "cell 0 0, grow");
		
		newInstanceButton = new JButton("Nowa instancja pojemnika");
		newInstanceButton.addActionListener((evt) -> {
			if(	classList.getSelectedIndex() > -1 &&
				constructorList.getSelectedIndex() > -1 &&
				setValueMethodList.getSelectedIndex() > -1 &&
				setWeightMethodList.getSelectedIndex() > -1	){
					
				Object box = newBoxInstance(
						modelClasses.get(classList.getSelectedIndex()),
						modelConstructors.get(constructorList.getSelectedIndex()),
						modelMethodSetWeight.get(setWeightMethodList.getSelectedIndex()),
						modelMethodSetValue.get(setValueMethodList.getSelectedIndex()));
				if(box != null){
					modelInstances.add(0, box);
				}
					
			}
		});
		rightPanel.add(newInstanceButton, "cell 0 1, grow");
		
		instancesList = new JList<Object>();
		
		JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem usun = new JMenuItem("Usuń");
		usun.addActionListener((evt) -> {
			modelInstances.remove(instancesList.getSelectedIndex());
		});
		
		popupMenu.add(usun);
		
		instancesList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
		       // if right mouse button clicked (or me.isPopupTrigger())
				if(SwingUtilities.isRightMouseButton(me)
		           && !instancesList.isSelectionEmpty()
		           && instancesList.locationToIndex(me.getPoint()) == instancesList.getSelectedIndex()) {
		               
					popupMenu.show(instancesList, me.getX(), me.getY());
		               
				}
		    }
		});
		modelInstances = new DefaultListModel<Object>();
		instancesList.setModel(modelInstances);
		instancesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		instancesListScrollPane = new JScrollPane(instancesList);
		
		rightPanel.add(instancesListScrollPane, "cell 0 2,grow");
		
		resultPanel = new JPanel();
		getContentPane().add(resultPanel, "cell 3 0,grow");
		resultPanel.setLayout(new MigLayout("", "[grow][]", "[][][grow]"));
		
		packLabel = new JLabel("Spakowane");
		resultPanel.add(packLabel, "cell 0 0,grow");
		
		packButton = new JButton("Pakuj plecak");
		packButton.addActionListener((evt)->{
			
			List<Object> boxes = new ArrayList<Object>();
			for(int i = 0; i < modelInstances.getSize(); i++){
				boxes.add(modelInstances.get(i));
			}
			
			NumberFormat format = NumberFormat.getInstance();
			format.setGroupingUsed(false);
		    NumberFormatter formatter = new NumberFormatter(format);
		    formatter.setValueClass(Integer.class);
		    formatter.setMinimum(0);
		    formatter.setMaximum(Integer.MAX_VALUE);
		    // If you want the value to be committed on each keystroke instead of focus lost
		    formatter.setCommitsOnValidEdit(true);
		    JFormattedTextField capacity = new JFormattedTextField(formatter);
		    
		    final JComponent[] inputs = new JComponent[] {
		    		new JLabel("Pojemność plecaka"),
		    		capacity
		    };
		    JOptionPane.showMessageDialog(null, inputs, "New Box instance", JOptionPane.PLAIN_MESSAGE);
		    
		    if(capacity.getText().length() > 0){
		    	
		    	packLabel.setText("Spakowane (dla plecaka o poj. "+capacity.getText()+")");
		    	
		    	modelResult.removeAllElements();
		    	
		    	knapsackPack(
						Integer.parseInt(capacity.getText()), 
						modelMethodGetWeight.get(getWeightMethodList.getSelectedIndex()),
						modelMethodGetValue.get(getValueMethodList.getSelectedIndex()),
						boxes)
						.stream().forEach((object)->{
							modelResult.add(0, object);
						});
		    }

		});
		
		resultPanel.add(packButton, "cell 0 1,grow");
		
		resultList = new JList<Object>();
		modelResult = new DefaultListModel<Object>();
		resultList.setModel(modelResult);
		resultList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		resultListLabel = new JScrollPane(resultList);
		
		resultPanel.add(resultListLabel, "cell 0 2,grow");
		

		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menu1 = new JMenu("Load Library");
		menuBar.add(menu1);
		
		JMenuItem menu1item1 = new JMenuItem("Load JAR");
		menu1item1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				JFileChooser chooser = new JFileChooser(); 
			    chooser.setCurrentDirectory(new java.io.File("."));
			    chooser.setDialogTitle("Choose library");
			    FileNameExtensionFilter ff = new FileNameExtensionFilter("Java JAR file", "jar");
			    chooser.addChoosableFileFilter(ff);
			    chooser.setFileFilter(ff);
			    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

			    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			    	modelClasses.removeAllElements();
			    	modelConstructors.removeAllElements();
			    	
			    	loadJar(new File(chooser.getSelectedFile().getAbsolutePath())).stream().forEach(
			    				(objClass) -> {
			    					modelClasses.add(0, objClass);
			    	});
			    }
			}
		});
		menu1.add(menu1item1);
	}
	

	protected abstract Collection<Object> 			knapsackPack(int capacity, Method getWeight, Method getValue, Collection<Object> boxes);

	protected abstract Object 						newBoxInstance(Class<?> classObject, Constructor<?> constructor, Method setWeight, Method setValue);

	protected abstract Collection<Class<?>> 		loadJar(File file);
	
	protected abstract Collection<Constructor<?>> 	filterModelConstructors(Class<?> classObject);
	
	protected abstract Collection<Method> 			filterModelGetValue(Class<?> classObject);
	
	protected abstract Collection<Method> 			filterModelGetWeight(Class<?> classObject);
	
	protected abstract Collection<Method> 			filterModelSetWeight(Class<?> classObject);

	protected abstract Collection<Method> 			filterModelSetValue(Class<?> classObject);
}
